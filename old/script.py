# Init du script avec les variables : chemin du fichier et extension
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

text = open("vulns.txt", 'r')
data = text.read().split('\n')
text.close()

for line in data:
    if line.startswith('High'):
        highVuln = line.split(':')[1].strip()  # Extract the value after the ':' character
        print(highVuln)
    if line.startswith('Medium'):
        medVuln = line.split(':')[1].strip()  # Extract the value after the ':' character
        print(medVuln)
    if line.startswith('Low'):
        lowVuln = line.split(':')[1].strip()  # Extract the value after the ':' character
        print(lowVuln)
    if line.startswith('Log'):
        logVuln = line.split(':')[1].strip()  # Extract the value after the ':' character
        print(logVuln)
    
font = font_manager.FontProperties(weight='bold', style='normal', size=10)

# Pie chart vulns
labels = 'Majeures', 'Importantes', 'Mineures', 'Info'
sizes = [highVuln, medVuln, lowVuln, logVuln]
explode = (0.1, 0, 0, 0)  # only "explode" the 1st slice
colors = ['firebrick','darkorange','gold','lightsteelblue']
fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '', colors=colors, shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
ax1.set_title("Vulnérabilités trouvées", weight='bold', style='normal', size=12)
ax1.legend(prop=font, loc='lower center', bbox_to_anchor=(0.05, -0.1), fancybox=True)
# Save image as png
plt.savefig("pieVulns.png", transparent=True)
