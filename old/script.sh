# On le tente en bash, parce bash.

# Parsing with bash, building with Python

# asking the path of the file
echo "Chemin vers le fichier"  
read pathtofile
echo "" > vulns.txt

if test -f "$pathtofile"; then
    echo "Le chemin vers le fichier est $pathtofile"
    echo "Quelle est l'extension du fichier (3 lettres en minuscule)?"  
    read extensiontype
    case $extensiontype in

    txt)
        # On compte le nombre d'hôtes totaux + le nombre de vulns par catégories
        echo "L'extension est au format .$extensiontype"
        # Total 
        totalhosts=$(cat $pathtofile | grep -i 'Total:' | awk '{print $2}')
        echo $totalhosts
        # Vulns "High"
        highvulns=$(cat $pathtofile | grep -i 'Total:' | awk '{print $3}')
        echo $highvulns
        # Vulns "Medium"
        medvulns=$(cat $pathtofile | grep -i 'Total:' | awk '{print $4}')
        echo $medvulns
        # Vulns "Low"
        lowvulns=$(cat $pathtofile | grep -i 'Total:' | awk '{print $5}')
        echo $lowvulns
        # Vulns "Log"
        logvulns=$(cat $pathtofile | grep -i 'Total:' | awk '{print $6}')
        echo $logvulns
        totalvulns=$(($highvulns + $medvulns + $lowvulns + $logvulns))
        echo "High :" $highvulns >> vulns.txt
        echo "Medium :" $medvulns >> vulns.txt
        echo "Low :" $lowvulns >> vulns.txt
        echo "Log :" $logvulns >> vulns.txt
        echo "Total Vulns :" $totalvulns >> vulns.txt
        echo "Hosts :" $totalhosts >> vulns.txt
        
        python script.py
        ;;
    csv)
        echo "L'extension est au format .$extensiontype"
        ;;

    xml)
        echo "L'extension est au format .$extensiontype"
        ;;

    *)
        echo "Je ne reconnais pas cette extension, désolé!"
        break
        ;;
    esac
else
echo "Je ne reconnais pas ce fichier, désolé!"
fi
