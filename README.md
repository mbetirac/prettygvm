# General purpose

# New

L'idée est de récupérer l'XML fourni par un scan OpenVAS pour le parser dans un dictionnaire (Json par exemple) pour intégration dans Sloth

## Old
L'idée d'un tel script est de générer un pseudo rapport des vulns trouvées par GVM dans un format plus synthéthique et 'human readable' car l'export au format en PDF ne semble pas fonctionner dans le conteneur.

Pour l'instant, seul le format .txt est supporté 
On scrappe les infos dans un script bash qui appelle un script python pour générer de joli graphiques avec **matplotlib**

Voici un exemple de graphique 

![chart](img/pieVulns.jpg)
